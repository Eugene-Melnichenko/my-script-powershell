﻿Add-Type -AssemblyName System.Windows.Forms
Add-Type -AssemblyName System.Drawing

Add-Type -AssemblyName System.Windows.Forms
Add-Type -AssemblyName System.Drawing

#__________________________________ФОРМА_ВВОДА_START__________________________________!
# https://docs.microsoft.com/ru-ru/powershell/scripting/samples/selecting-items-from-a-list-box?view=powershell-7.1

$form = New-Object System.Windows.Forms.Form
$form.Text = 'Виберіть usb пристрій' #заголовком окна
$form.Size = New-Object System.Drawing.Size(400,200) #размер формы в пикселях
$form.StartPosition = 'CenterScreen' #автоматически отображаться в центре экрана при загрузке

#Кнопка1
$okButton = New-Object System.Windows.Forms.Button
$okButton.Location = New-Object System.Drawing.Point(100,120)
$okButton.Size = New-Object System.Drawing.Size(75,23)
$okButton.Text = 'OK'
$okButton.DialogResult = [System.Windows.Forms.DialogResult]::OK
$form.AcceptButton = $okButton
$form.Controls.Add($okButton)
#Кнопка2
$cancelButton = New-Object System.Windows.Forms.Button
$cancelButton.Location = New-Object System.Drawing.Point(225,120)
$cancelButton.Size = New-Object System.Drawing.Size(75,23)
$cancelButton.Text = 'Cancel'
$cancelButton.DialogResult = [System.Windows.Forms.DialogResult]::Cancel
$form.CancelButton = $cancelButton
$form.Controls.Add($cancelButton)
#Текст метки в окне
$label = New-Object System.Windows.Forms.Label
$label.Location = New-Object System.Drawing.Point(10,20)
$label.Size = New-Object System.Drawing.Size(380,20)
$label.Text = 'Для перевірки введыть адресу носій інформації:'
$form.Controls.Add($label)
#текстовое поле
$listBox = New-Object System.Windows.Forms.ListBox
$listBox.Location = New-Object System.Drawing.Point(10,40)
$listBox.Size = New-Object System.Drawing.Size(360,20)
$listBox.Height = 80


#Дані Локальниї дисків які підключені до комп'ютера HDD, SDD, FLASH, CD-DVD.....
$localDisk = Get-Partition | Where-Object -FilterScript {$_.DriveLetter -Eq "A" -or $_.DriveLetter -Eq "B" -or $_.DriveLetter -Eq "C" -or $_.DriveLetter -Eq "D" -or
                                                 $_.DriveLetter -Eq "E" -or $_.DriveLetter -Eq "F" -or $_.DriveLetter -Eq "H" -or $_.DriveLetter -Eq "I" -or
                                                 $_.DriveLetter -Eq "J" -or $_.DriveLetter -Eq "K" -or $_.DriveLetter -Eq "L" -or $_.DriveLetter -Eq "M" -or
                                                 $_.DriveLetter -Eq "N" -or $_.DriveLetter -Eq "O" -or $_.DriveLetter -Eq "P" -or $_.DriveLetter -Eq "Q" -or
                                                 $_.DriveLetter -Eq "R" -or $_.DriveLetter -Eq "S" -or $_.DriveLetter -Eq "T" -or $_.DriveLetter -Eq "U" -or
                                                 $_.DriveLetter -Eq "V" -or $_.DriveLetter -Eq "W" -or $_.DriveLetter -Eq "X" -or $_.DriveLetter -Eq "Y" -or $_.DriveLetter -Eq "Z"
}


#Поле вибору пристрою(диска)
For ($i=0; $i –lt $localDisk.Count; $i++) { 
    $typeDisk = " Type: " + $localDisk[$i].Type #Тип флешки
    $sizeDisk = "Size: " + [math]::Round($localDisk[$i].Size/1073741824, 3) + " ГБ" #Розмір флешки
    [void] $listBox.Items.Add($localDisk[$i].DriveLetter + ":\ `t|" + $typeDisk + "/" + $sizeDisk) 
}

$form.Controls.Add($listBox)
$form.Topmost = $true
#Для отображения формы в Windows.
$result = $form.ShowDialog()
#__________________________________ФОРМА_ВВОДА_END__________________________________!

$path_to_usb = $null
$path_to_usb = $listBox.Text #Путь который будем использовать для зчитування даних
$path_to_usb = $path_to_usb.Remove(3)
$path_to_usb += "test"


$global:text_in_line = @($null) #Для хранения всех строк документа
$global:name_file = @($null) #Для хранения путей к файлам
$find_name = @('дск','для службового користування','таємно','цілком таємно') #Слова які використовуємо для пошуку

#Збираємо дані про ПК
function getDatePK {
    $global:text_in_line += "User name: $env:UserName"
    $global:text_in_line += "=" * 75 #======================================Просто строка в тексте для лучшей визуализации!
    #Дані ОС та ПЕОМ
    $systeminfo = systeminfo /FO csv | ConvertFrom-Csv
    $global:text_in_line += "OS Name:`t" + $systeminfo.'OS Name'
    $global:text_in_line += "System Type:`t" + $systeminfo.'System Type'
    $global:text_in_line += "Model :`t" + $systeminfo.'System Manufacturer' + ' ' + $systeminfo.'System Model'
    #Дані Wi-Fi
    $global:text_in_line += "Wi-Fi:"
    $global:text_in_line += "`t InterfaceDescription: "+ (Get-NetAdapter -Name "Wi-Fi").InterfaceDescription
    $global:text_in_line += "`t MacAddress: "+ (Get-NetAdapter -Name "Wi-Fi").MacAddress
    #Дані Ethernet
    $global:text_in_line += "Ethernet:"
    $global:text_in_line += "`t InterfaceDescription: "+ (Get-NetAdapter -Name "Ethernet").InterfaceDescription
    $global:text_in_line += "`t MacAddress: "+ (Get-NetAdapter -Name "Ethernet").MacAddress
    $global:text_in_line += "=" *75 #======================================Просто строка в тексте для лучшей визуализации!
}


#Збираємо адреса файлів, які мають в імені одне слів $find_name
function getAllParh_File {
<#  ПЕРЕВІРКА ФАЙЛІВ НА НАЯВНСТЬ СЛІВ, що знаходиться у $find_name
     Get-ChildItem = предназначен специально для возврата всех элементов, найденных в контейнере, например в папке.
    -Force = Принудительное перечисление скрытых элементов 
    -Name = Фильтрация элементов по имени 
    -$global:path_to_usb = маршрут до каталогу #>
    $AllFileInPath = Get-ChildItem -Path $path_to_usb -Name -Recurse -Force    
        
    #Перевіряємо всі маршрути до файлів на явінсть слів $find_name
    For ($i=0; $i –lt $AllFileInPath.Length; $i++) { 
      
        $OneSt= $AllFileInPath[$i].ToLower() #Строка в нижнем регистре
        if (($OneSt.IndexOf($find_name[0]) -ne -1) -or 
            ($OneSt.IndexOf($find_name[1]) -ne -1) -or 
            ($OneSt.IndexOf($find_name[2]) -ne -1) -or 
            ($OneSt.IndexOf($find_name[3]) -ne -1)
           ) {
             #Якщо строку знайдено то...
             $global:name_file += $OneSt
          }
   }
}


#________________________________CREATE_DOC_START________________________________!
#Что следует делать с формой после того, как пользователь выберет параметр ОК 
if ($result -eq [System.Windows.Forms.DialogResult]::OK) {

    #Теперішній час, ім'я файлу!
    $date = (Get-Date -f dd-MM-yyyy_HH-mm-ss).ToString() #Теперішній час, ім'я файлу!
    $text_in_line += "Документ створено:`t $date" 

    #Збирає дані про ПК
    getDatePK 
    #Отримуємо всі адреси файлів де знайдено слова з $find_name 
    getAllParh_File

    #Якщо файли знайдено 
    if($name_file.Length -gt 30){
        $text_in_line += $name_file #додаємо маршрути до тексту створеного файлу!

        #Створюємо файл за адресою!
        $nameFile = 'C:\my-program\' + $date +".txt"
        New-Item -Force -Path $nameFile -ItemType File
        #Записуєио дані до файлу!
        Add-Content -Path $nameFile -Encoding "UTF8" $text_in_line
    } else {
        $wshell = New-Object -ComObject Wscript.Shell
        $Output = $wshell.Popup("На USB носії що підключено до ПЕОМ не знайдено файл що має гриф обмеження доступу.")
    }



}